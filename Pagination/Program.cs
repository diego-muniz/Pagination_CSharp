﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pagination
{
  class Program
  {
    static void Main(string[] args)
    {
      List<int> lst = new List<int>();

      for (int i = 1; i <= 100; i++)
      {
        lst.Add(i);
      }

      IList<int> thirdPage = GetPage(lst, 3, 10);


    }
    static IList<int> GetPage(IList<int> list, int page, int pageSize)
    {
      return list.Skip(page * pageSize).Take(pageSize).ToList();
    }
  }
}
